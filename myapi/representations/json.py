"""
    Representation extension for Restful flask.
"""
from flask import make_response
from mongoengine import Document, QuerySet
import flask_restful.representations.json as default


def output_json(data, code, headers=None):
    """ Use mongoengine json serialization if Document or QuerySet. """
    if isinstance(data, (Document, QuerySet)):
        resp = make_response(data.to_json(), code)
        resp.headers.extend(headers or {})
        return resp

    return default.output_json(data, code, headers)
