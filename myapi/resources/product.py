""" product resource """

from flask import request
from flask_restful import Resource, reqparse
from flasgger import swag_from
from mongoengine import Document, StringField


class ProductDoc(Document):
    name = StringField(max_length=60)
    description = StringField()


class ProductList(Resource):
    """ Handle creation and listing of products """

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('limit', type=int, location='args', default=10)
        self.parser.add_argument('offset', type=int, location='args', default=0)

    @swag_from('product_list.yml')
    def get(self):
        params = self.parser.parse_args()
        return ProductDoc.objects.skip(params.offset).limit(params.limit).only(
            'name', 'description'), 200

    @swag_from('product_list_post.yml', validation=True)
    def post(self):
        product = ProductDoc(**request.json)
        product.save()
        return product, 201
