""" You say hello I say good by. """
from flask_restful import Resource
from flasgger import swag_from


class Hello(Resource):
    """ The Hello resource. """

    @swag_from('hello.yml')
    def get(self, name=''):
        return "Hello %s!" % name

    def post(self):
        raise NotImplementedError('Nothing here yet')
