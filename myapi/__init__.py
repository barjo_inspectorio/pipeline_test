"""
    A dummy api app.
    Use mongodb as the default db.
    Use swagger for api doc and validation.
    Use flask_restful for resource handling.
"""

from os import environ
from flask import Flask, make_response
from flask_restful import Api
from flasgger import Swagger
from flask_mongoengine import MongoEngine
from myapi.representations.json import output_json

# Resources
from myapi.resources.hello import Hello
from myapi.resources.product import ProductList


def create_app():
    """ Create the flask app. """
    app = Flask(__name__)
    # Default settings from env.
    app.config.from_pyfile('../conf/default_settings.py')

    init_db(app)
    init_swagger(app)
    init_api(app)
    return app


def init_swagger(app):
    app.swag = Swagger(app)


def init_db(app):
    app.db = MongoEngine()
    app.db.init_app(app)


def init_api(app):
    api = Api(app)
    # Override default json representation
    api.representations['application/json'] = output_json

    # Routes
    api.add_resource(Hello, '/hello', '/hello/', '/hello/<string:name>')
    api.add_resource(ProductList, '/products')
