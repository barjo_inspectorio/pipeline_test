FROM python:3

WORKDIR /usr/src/app
COPY requirements.txt ./
COPY myapi/ ./myapi
COPY conf/ ./conf
RUN apt-get update -y
RUN pip install --no-cache-dir -r requirements.txt

ENV GUNICORN_WORKERS 7
ENV GUNICORN_WORKERS_CLASS gevent
ENV GUNICORN_BIND 0.0.0.0:8000
ENV GUNICORN_ACCESSLOG -
ENV FLASK_DEBUG 0
ENV FLASK_TESTING 0

EXPOSE 8000
ENTRYPOINT ["gunicorn", "--config", "conf/gunicorn.py", "--log-config", "conf/logging.conf", "myapi.app:app"]
