import json

def decode(data):
    return json.loads(data.decode("utf-8"))
