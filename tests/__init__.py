import unittest
from os import environ
from flask import current_app
import myapi

class ApiTestCase(unittest.TestCase):

    def setUp(self):
        environ['MONGODB_HOST'] = 'mongomock://localhost'
        app = myapi.create_app()
        app.testing = True
        self.client = app.test_client()
