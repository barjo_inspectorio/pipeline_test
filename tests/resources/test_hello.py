import tests

class HelloTestCase(tests.ApiTestCase):

    def test_hello_bob(self):
        resp = self.client.get('/hello/bob')
        assert b'Hello bob!' in resp.data
