""" Product resource test suite """

from myapi.resources.product import ProductDoc
from tests import ApiTestCase
from tests.utils import decode

class ProductListTestCase(ApiTestCase):

    def test_get_products(self):
        # Given one product
        product = ProductDoc(name='Ha', description='Ho')
        product.save()
        # When calling without query params
        resp = self.client.get('/products')
        json = decode(resp.data)
        # Then
        self.assertEqual(len(json), 1)
        self.assertEqual(json[0]['name'], product.name)
