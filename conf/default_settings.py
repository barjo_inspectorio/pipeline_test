"""
    Load flask default settings from environment variables
"""
import os


locals()['DEBUG'] = True
locals()['TESTING'] = True

# Extract mongobd configuration from environment variables
# MONGODB_HOST           -> the database host
# For more information check https://flask-mongoengine.readthedocs.io/en/latest/
for k, v in os.environ.items():
    if k.startswith("MONGODB_"):
        locals()[k] = v
    elif k.startswith('FLASK_'): # override from env variables
        key = k.split('_', 1)[1].lower()
        locals()[key] = v
