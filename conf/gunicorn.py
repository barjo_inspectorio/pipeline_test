"""
 Extract gunicorn configuration from environment variables
 GUNICORN_WORKERS       -> number of workers
 GUNICORN_WORKERS_CLASS -> workers class (gevent/sync..)
 GUNICORN_BACKLOG       -> number of connection in the backlogs
 GUNICORN_BIND          -> binding address (0.0.0.0:8000)
 for more see: http://docs.gunicorn.org/en/stable/settings.html#config-file
"""
import os

for k, v in os.environ.items():
    if k.startswith("GUNICORN_"):
        key = k.split('_', 1)[1].lower()
        locals()[key] = v
