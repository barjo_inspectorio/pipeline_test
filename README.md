# Pipeline test

The purpose of this project is to test out bitbucket build pipelines as well as various python tools.

## Set-Up

### Install dependencies

```
virtualenv venv             # better to use virtualenv
source venv/bin/activate
pip install -r requirements.txt
```

### Start a default mongodb

```
docker run -p 27017:27017 --name some-mongo
```

### Run flask

```
export FLASK_DEBUG=1
export FLASK_APP='myapi/app.py'
flask run
```

## Tools

### Server

 * gunicorn with gevent

### Logging

 * loggging (error) in formated as json and given to the console

### Unit Test

 * unittest

### Static check

 * pylint (linter)
 * bandit (security scan)

### Documentation

 * ? sphinx ? autodoc
 * ? ReadTheDoc ?
